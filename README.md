# MPI Introduction

The following exercise will introduce the most basic MPI communication patterns. For this, write an MPI program
(`communications.cpp`) that is composed of the following steps:
1. Each process generates an integer between 0,...,99. Every process will sent the number to one root process, 
   e.g. the process with rank 0. The root process prints all numbers, sorted by the rank which sent it. 
   ('all-to-one' communication)
2. The process with rank i computes the absoulte difference between its number and the number of rank i-1. Rank 0
   compares its number with the process.
   ('point-to-point' communication)
3. Finally, the maximal difference between the number of two processes is computed, i.e. the rank i computes the 
   difference to i-1 and i+1. The result will be sent to all processes and the process which attains the maximal
   difference will print a message.
   ('all-to-all' communication)

You can find information on the necessary MPI functions under:
- https://rookiehpc.com/mpi/docs/index.php
- https://www.mpi-forum.org/docs/
- https://www.mpich.org/static/docs/latest/
- http://kayaogz.github.io/teaching/app4-programmation-parallele-2018/cours/MPI-cheatsheet.pdf

Optinal tasks:
- Use non-blocking communications.
- Create virutal topologies (both cartisian and graph) and use neighbour hood communication.
