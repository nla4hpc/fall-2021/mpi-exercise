#include <iostream>
#include <mpi.h>

int main(int argc, char* argv[]) {
  MPI_Init(&argc, &argv);  // initialize MPI, this MUST be places before any other MPI function call

  /**
   * Determine the rank of this process (process id). The rank is a unique number between 0,...,P-1, where P is the
   * number of available processes. MPI_COMM_WORLD is a predefined constant, that denotes the group of all processes.
   */
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  std::cout << "Hello, World from " << rank << std::endl;

  MPI_Finalize();  // Finalize MPI, this MUST be executed in all MPI programms. No MPI function calls are allowed afterwards.
}